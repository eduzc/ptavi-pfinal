# /usr/bin/python3
# -*- coding: utf-8 -*-

import os
import sys
import time
import socket

from xml.sax import make_parser
from proxy_registrar import XMLHandler, LOG, get_digest
# poner raise ...
# reemplazar valores del pdf
att = {'account': ['username', 'passwd'],
       'uaserver': ['ip', 'puerto'],
       'rtpaudio': ['puerto'],
       'regproxy': ['ip', 'puerto'],
       'log': ['path'],
       'audio': ['path']}
# peticion de argumentos
usage_error = 'Usage: python uaclient.py config method option'
methods_allowed = 'register, invite, bye, ack'
# tipos de errores. comprobar
ERROR_400 = 'ERROR_400: ' + 'Bad request received' 
ERROR_401 = 'ERROR_401: ' + 'SIP/2.0 401 Unauthorized' 
ERROR_404 = 'ERROR 404: ' + 'User not found'
ERROR_405 = 'ERROR_405: ' + 'Method not allowed' 


if len(sys.argv) != 4:
    sys.exit(usage_error)
else:
    if os.path.exists(sys.argv[1]):
        xml_file = sys.argv[1]
        method = sys.argv[2]
        option = sys.argv[3]
    else:
        sys.exit('file ' + sys.argv[1] + ' not found')

class SIPMessages:

    def __init__(self, user, port, ip, rtp_port):
        self.src = {'user': user, 'ip': ip, 'port': port, 'rtp_port': rtp_port}

    def register(self, option):
        line = 'REGISTER sip:' + self.src['user'] + ':' + self.src['port']
        line += ' SIP/2.0\r\nExpires: ' + option + '\r\n'

        return line

    def invite(self, option):
        line = 'INVITE sip:' + option + ' SIP/2.0\r\n'
        line += 'Content-Type: application/sdp\r\n\r\nv=0\r\n'
        line += 'o=' + self.src['user'] + ' ' + self.src['ip'] + '\r\n'
        line += 's=My_Session\r\nt=0\r\n'
        line += 'm=audio ' + self.src['rtp_port'] + ' RTP\r\n'

        return line

    def bye(self, option):
        line = 'BYE sip:' + option + ' SIP/2.0\r\n'

        return line

    def ack(self, option):
        line = 'ACK sip:' + option + ' SIP/2.0\r\n'

        return line

    def get_mess(self, method, option):
        print("Enviamos al Proxy:")
        #colocar un and con option == 0
        if str.lower(method) == 'register':
            petition = ("REGISTER SIP/2.0: " + user + "\r\n" )
            if option == "3600":
              petition += ("Port:" + port + "\r\n" + "EXPIRES = 3600")
              print(petition)
            elif option == "0":
              petition += ("SIGN OUT" + "\r\n" + "EXPIRES = 0")
              print(petition)
              print("\r\n")
              print("El usuario " + user +" se ha dado de baja" )
            return self.register(option)
        elif str.lower(method) == 'invite':
            line = 'INVITE sip:' + option + ' SIP/2.0\r\n'
            line += 'Content-Type: application/sdp\r\n\r\nv=0\r\n'
            line += 'o=' + self.src['user'] + ' ' + self.src['ip'] + '\r\n'
            line += 's=My_Session\r\nt=0\r\n'
            line += 'm=audio ' + self.src['rtp_port'] + ' RTP\r\n'
            print(line)

            return self.invite(option)
        elif str.lower(method) == 'bye':
            line = 'BYE sip:' + option + ' SIP/2.0\r\n'
            print(line)
            return self.bye(option)
        elif str.lower(method) == 'ack':
            line = 'ACK sip:' + option + ' SIP/2.0\r\n'
            print(line)
            return self.ack(option)

if __name__ == '__main__':

    parser = make_parser()
    xml_list = XMLHandler(att)
    parser.setContentHandler(xml_list)
    parser.parse(open(xml_file))
    tags = xml_list.get_tags()
    log = LOG(tags['log_path'])

    user = tags['account_username']
    psswd = tags['account_passwd']
    port = tags['uaserver_puerto']
    ip = tags['uaserver_ip']
    user_address = user + ':' + port
    rtp_port = tags['rtpaudio_puerto']
    pr_ip = tags['regproxy_ip']
    pr_port = tags['regproxy_puerto']
    pr_address = pr_ip + ':' + pr_port
    sip_mess = SIPMessages(user, port, ip, rtp_port)
    log.starting()

    if str.lower(method) in methods_allowed:
        line = sip_mess.get_mess(str.lower(method), option) + '\r\n'

        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
            my_socket.connect((pr_ip, int(pr_port)))
            my_socket.send(bytes(line, 'utf-8'))

            log.sent_to(pr_address, line)

            try:
                data = my_socket.recv(1024).decode('utf-8')
                log.received_from(pr_address, data)
            except:
                #probar con un or
                log.error('Connection refused, check the servers connection')
                sys.exit('Connection refused, check the servers connection')
            print("\r\n" + "Recibimos del Proxy: ")
            if '100' in data:
                print('SIP/2.0 100 Trying')
                if '180' in data:
                    print('SIP/2.0 180 Ringing')
                    if '200' in data:
                        print('SIP/2.0 200 OK')
                        sdp = data.split('\r\n\r\n')[4]
                        ip = sdp.split('\r\n')[1].split()[-1]
                        port = sdp.split('\r\n')[4].split()[1]
                        mp32rtp = './mp32rtp -i ' + ip + ' -p ' + port
                        mp32rtp += ' < ' + tags['audio_path']
                        line = sip_mess.get_mess('ack', option) + '\r\n'
                        cvlc = 'cvlc rtp://@' + ip + ':' + port
                        my_socket.send(bytes(line, 'utf-8'))
                        log.sent_to(pr_address, line)
                        print('running: ' + mp32rtp)#
                        os.system(mp32rtp)#
                        print('running: ' + cvlc + ' && ' + mp32rtp)
                        os.system(cvlc + ' && ' + mp32rtp)
            elif '200' in data:
                print('200 OK received')
            elif '400' in data:
                print(ERROR_400)
            elif '401' in data:
                message  = ERROR_401 + "\r\n"
                message += "WWW Authenticate: Digest nonce = 898989898798989898989 " 
                message += "\r\n" + 'Authorization Required' + "\r\n"
                message += "Please, Try again:"
                print(message)
                #print("SIP/2.0 401 Unauthorized")
                #print("WWW Authenticate: Digest nonce = 898989898798989898989 ")
                #print('Authorization Required')
                #print("Please, Try again:")
                print("\r\n")
                nonce = data.split('"')[1]
                response = get_digest(nonce, psswd)
                auth = 'Authorization: Digest response="' + response + '"'
                line = sip_mess.get_mess(str.lower(method), option)
                line += auth
                line += "\r\n\r\n"
                print(line)
                #print("\r\n")
                print("El usuario " + user + " se ha dado de alta")
                my_socket.send(bytes(line, 'utf-8'))

                log.sent_to(pr_address, line)

                try:
                    data = my_socket.recv(1024).decode('utf-8')
                except:
                    log.error('Connection refused')
                    sys.exit('Connection refused')

                log.received_from(pr_address, data)

                if '200' in data:
                    print("\r\n")
                    print("Recibimos del Proxy: ")
                    print('SIP/2.0 200 OK')
                else:
                    print(data)

            elif '404' in data:
                print(ERROR_404)
            elif '405' in data:
                print(ERROR_405)

    else:
        log.error('Method not allowed')
        sys.exit(ERROR_405)

    log.finishing()
